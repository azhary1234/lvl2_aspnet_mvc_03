﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03.Models;
using System.Data.Entity;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        private Db_CustomerEntities dbModel = new Db_CustomerEntities();

        // GET: Customer
        public ActionResult Index()
        {
            return View(dbModel.Tbl_Customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            var customer = dbModel.Tbl_Customer.Where(c => c.Id == id).FirstOrDefault();

            if (customer == null)
                return HttpNotFound("We didn't find that action, sorry!");

            return View(customer);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Tbl_Customer customer)
        {
            using (var transactions = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    //Create - Insert new Customer
                    dbModel.Tbl_Customer.Add(customer);
                    dbModel.SaveChanges();

                    //Edit - Edit Existing Customer with Id = 1
                    var edit = dbModel.Tbl_Customer.Where(c => c.Id == 1).FirstOrDefault();
                    edit.Description = "Jakarta Barat";
                    
                    dbModel.Entry(edit).State = EntityState.Modified;
                    dbModel.SaveChanges();
                    transactions.Commit(); //hanya di commit

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    //(string)Session["Username"]
                    Tbl_Log_History error = new Tbl_Log_History
                    {
                        ErrorMessage = e.Message,
                        ErrorPosition = "Create_Customer",
                        ErrorDate = DateTime.Now
                    };

                    dbModel.Tbl_Log_History.Add(error);
                    dbModel.SaveChanges();
                    transactions.Rollback(); //Jika ada error, tidak ditambah dan akan di rollback

                    ViewBag.MsgError = e.Message;

                    return View();
                }
            }

        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add update logic here
                dbModel.Entry(customer).State = EntityState.Modified;
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            var customer = dbModel.Tbl_Customer.Where(c => c.Id == id).FirstOrDefault();
            return View(customer);
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = dbModel.Tbl_Customer.Where(c => c.Id == id).FirstOrDefault();
                dbModel.Tbl_Customer.Remove(customer);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
